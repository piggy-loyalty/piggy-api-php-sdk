<?php

namespace Tests\Piggy\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;
use PHPUnit\Framework\TestCase;
use Piggy\Api\PiggyApi;
use ReflectionClass;

class PiggyTestCase extends TestCase
{
    /**
     * @param $expected
     * @param bool $isError
     * @param bool $isAuthenticated
     * @return PiggyApi
     * @throws \ReflectionException
     */
    protected function mockHttpClient($expected, $isError = false)
    {
        $stream = Psr7\stream_for( json_encode($expected) );
        $response = new Response(
            200,
            ['Content-Type' => 'application/json'],
            Psr7\stream_for($stream)
        );

        if($isError) {
            $response = $expected;
        }

        $mock = new MockHandler([$response]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $piggyApi = new PiggyApi("1", "1");

        $reflection = new ReflectionClass($piggyApi);
        $reflectionProperty = $reflection->getProperty('httpClient');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($piggyApi, $mockClient);

        return $piggyApi;
    }

    protected function createExceptionForMockedClient($status, $title, $message)
    {
        $message = "Error executing API call ({$status}) {$title}: {$message}";
        $e = new RequestException($message, new Request('POST', 'https://api.piggyloyalty.nl/'));

        return $e;
    }
}