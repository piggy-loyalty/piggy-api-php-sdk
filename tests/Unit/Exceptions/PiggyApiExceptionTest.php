<?php

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Piggy\Api\Exceptions\PiggyApiException;
use Tests\Piggy\Api\PiggyTestCase;

class PiggyApiExceptionTest extends  PiggyTestCase
{
    /**
     * @test
     * @throws PiggyApiException
     */
    public function create_exception_from_guzzle_request_exception()
    {
        $e = new RequestException('Request Failed', new Request('GET', 'https://api.piggyloyalty.nl/'));

        $piggyApiException = PiggyApiException::createFromGuzzleException($e);

        $this->assertInstanceOf(PiggyApiException::class, $piggyApiException);
        $this->assertNull($piggyApiException->getResponse());
    }
}