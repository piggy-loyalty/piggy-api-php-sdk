<?php

use Tests\Piggy\Api\PiggyTestCase;

class CreditBalancesResourceTest extends PiggyTestCase
{
    /** @test */
    public function it_returns_credit_balances()
    {
        $piggyApi = $this->mockHttpClient(["data" => "200"]);

        $piggyApi->setAccessToken('1');
        $piggyApi->setClientToken('1');

        $actual = $piggyApi->customer->creditBalances->show("1");

        $this->assertEquals(200, $actual);
    }
}