<?php

use GuzzleHttp\Psr7\Response;
use Piggy\Api\Model\StagedCreditReception;
use Tests\Piggy\Api\PiggyTestCase;

class StagedCreditReceptionsResourceTest extends PiggyTestCase
{

    /** @test */
    public function it_returns_an_staged_credit_reception()
    {
        $obj = new stdClass();
        $obj->hash = "asd123";
        $obj->credits = 100;

        $piggyApi = $this->mockHttpClient(["data" => $obj]);
        $piggyApi->setClientToken('1');

        $actual = $piggyApi->client->stagedCreditReceptions->create(1, "100");

        $stagedCreditReception = new StagedCreditReception();
        $stagedCreditReception->setHash($obj->hash);
        $stagedCreditReception->setCredits($obj->credits);
        $stagedCreditReception->setCreditReception(null);
        $stagedCreditReception->setCreatedAt("");

        $this->assertInstanceOf(StagedCreditReception::class, $actual);
        $this->assertEquals($stagedCreditReception, $actual);
    }

    /** @test */
    public function it_returns_a_response_after_sending_staged_credit_reception()
    {
        $piggyApi = $this->mockHttpClient([]);

        $piggyApi->setClientToken("123");

        $actual = $piggyApi->client->stagedCreditReceptions->send("1","2");

        $this->assertInstanceOf(Response::class, $actual);
    }

}
