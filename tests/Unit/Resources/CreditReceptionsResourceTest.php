<?php

use Piggy\Api\Model\CreditReception;
use Piggy\Api\Model\Customer;
use Tests\Piggy\Api\PiggyTestCase;

class CreditReceptionsResourceTest extends PiggyTestCase
{

    /** @test */
    public function it_returns_an_credit_reception()
    {
        $customerObj = new stdClass();
        $customerObj->id = 1;
        $customerObj->email = "piet@jan.nl";

        $obj = new stdClass();
        $obj->id = 1;
        $obj->credits = 100;
        $obj->member = $customerObj;
        $obj->purchase_amount = null;
        $obj->created_at = null;

        $piggyApi = $this->mockHttpClient(["data" => $obj]);
        $piggyApi->setClientToken('1');

        $actual = $piggyApi->client->creditReceptions->create("1", "100", "1");

        $customer = new Customer();
        $customer->setId(1);
        $customer->setEmail("piet@jan.nl");

        $creditReception = new CreditReception();
        $creditReception->setId($obj->id);
        $creditReception->setCredits($obj->credits);
        $creditReception->setCustomer($customer);

        $this->assertInstanceOf(Customer::class, $actual->getCustomer());
        $this->assertInstanceOf(CreditReception::class, $actual);
        $this->assertEquals($creditReception, $actual);
    }
}