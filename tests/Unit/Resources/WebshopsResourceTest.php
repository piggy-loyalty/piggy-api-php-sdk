<?php

namespace Tests\Unit\Resources;

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Model\LoyaltyProgram;
use Piggy\Api\Model\Webshop;
use ReflectionException;
use Tests\Piggy\Api\PiggyTestCase;

class WebshopsResourceTest extends PiggyTestCase
{

    /**
     * @test
     * @throws ReflectionException
     * @throws PiggyApiException
     */
    public function it_returns_all_webshops()
    {
        $obj1 = new \stdClass();
        $obj1->id = 1;
        $obj1->name = "webshop1";
        $obj1->loyalty_program = null;

        $obj2 = new \stdClass();
        $obj2->id = 2;
        $obj2->name = "webshop2";
        $obj2->loyalty_program = null;

        $input = ["data" => [$obj1, $obj2]];

        $webshop1 = new Webshop();
        $webshop1->setId($obj1->id);
        $webshop1->setName($obj1->name);
        $webshop1->setLoyaltyProgram(null);

        $webshop2 = new Webshop();
        $webshop2->setId($obj2->id);
        $webshop2->setName($obj2->name);
        $webshop2->setLoyaltyProgram(null);

        $piggyApi = $this->mockHttpClient($input);
        $piggyApi->setClientToken('1');

        $actual = $piggyApi->client->webshops->index();

        $this->assertInstanceOf(Webshop::class, $actual[0]);
        $this->assertEquals([$webshop1, $webshop2], $actual);
    }

    /**
     * @test
     * @throws ReflectionException
     * @throws PiggyApiException
     */
    public function it_returns_one_webshop_by_id()
    {
        $obj = new \stdClass();
        $obj->id = 2;
        $obj->name = "webshop2";
        $obj->loyalty_program = null;

        $input = ["data" => $obj];

        $webshop = new Webshop();
        $webshop->setId($obj->id);
        $webshop->setName($obj->name);
        $webshop->setLoyaltyProgram(null);

        $piggyApi = $this->mockHttpClient($input);
        $piggyApi->setClientToken('1');

        $actual = $piggyApi->client->webshops->show(2);

        $this->assertInstanceOf(Webshop::class, $actual);
        $this->assertEquals($webshop, $actual);
    }

    /** @test */
    public function it_returns_one_webshop_with_loyalty_program_by_id()
    {
        $loyaltyProgramObj = new \stdClass();
        $loyaltyProgramObj->id = 1;
        $loyaltyProgramObj->name = "loyaltyProgram1";

        $obj = new \stdClass();
        $obj->id = 2;
        $obj->name = "webshop2";
        $obj->loyalty_program = $loyaltyProgramObj;

        $input = ["data" => $obj];

        $loyaltyProgram = new LoyaltyProgram();
        $loyaltyProgram->setId($loyaltyProgramObj->id);
        $loyaltyProgram->setName($loyaltyProgramObj->name);

        $webshop = new Webshop();
        $webshop->setId($obj->id);
        $webshop->setName($obj->name);
        $webshop->setLoyaltyProgram($loyaltyProgram);

        $piggyApi = $this->mockHttpClient($input);
        $piggyApi->setClientToken('1');

        $actual = $piggyApi->client->webshops->show(2);

        $this->assertInstanceOf(Webshop::class, $actual);
        $this->assertEquals($webshop, $actual);
    }
}