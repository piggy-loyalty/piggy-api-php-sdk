<?php

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Model\Customer;
use Piggy\Api\Model\OAuthToken;
use Piggy\Api\PiggyApi;
use Tests\Piggy\Api\PiggyTestCase;

class CustomerTest extends PiggyTestCase
{

    /** @test */
    public function it_sets_the_redirect_uri_when_generating_authentication_url()
    {
        $redirectUri = "http://localhost:3030";

        $piggyApiDev = new PiggyApi('1', '1', 'development');

        $piggyApiDev->customer->generateAuthenticationUrl($redirectUri);

        $this->assertEquals($redirectUri, $piggyApiDev->customer->getRedirectUri());
    }
    /** @test */
    public function it_returns_authorization_url_for_development_environment()
    {
        $clientId = "1";
        $clientSecret = "1";
        $redirectUri = "http://localhost:3030";

        $piggyApiDev = new PiggyApi($clientId, $clientSecret, 'development');

        $authUrl = $piggyApiDev->customer->generateAuthenticationUrl($redirectUri);

        $this->assertEquals($redirectUri, $piggyApiDev->customer->getRedirectUri());
        $this->assertEquals("https://customer.piggyloyalty.nl/oauth?client_id={$clientId}&redirect_uri={$redirectUri}&response_type=code&scope=", $authUrl);
    }

    /** @test */
    public function it_returns_authorization_url_for_production_environment()
    {
        $clientId = "1";
        $clientSecret = "1";
        $redirectUri = "http://localhost:3030";

        $piggyApiDev = new PiggyApi($clientId, $clientSecret, 'production');

        $authUrl = $piggyApiDev->customer->generateAuthenticationUrl($redirectUri);

        $this->assertEquals("https://customer.piggy.nl/oauth?client_id={$clientId}&redirect_uri={$redirectUri}&response_type=code&scope=", $authUrl);
    }

    /**
     * @test
     * @throws ReflectionException
     * @throws PiggyApiException
     */
    public function it_throws_piggy_api_exception_when_customer_authentication_failed()
    {
        $this->expectException(PiggyApiException::class);
        $this->expectExceptionMessage("The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.");

        $e = $this->createExceptionForMockedClient(400, "invalid_request", "The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.");

        $piggyApi = $this->mockHttpClient($e, true);

        $piggyApi->client->requestClientToken();
    }

    /**
     * @test
     * @throws PiggyApiException
     * @throws ReflectionException
     */
    public function it_returns_access_token_for_customers()
    {
        $redirectUri = "http://localhost:3030";

        $token = new OAuthToken();
        $token->setAccessToken("123");
        $token->setRefreshToken("123");
        $token->setExpiresIn("123");

        $expects = ["access_token" => "123", "refresh_token" => "123", "expires_in" => "123"];

        $piggyApi = $this->mockHttpClient($expects);

        $piggyApi->customer->setRedirectUri($redirectUri);
        $actual = $piggyApi->customer->requestAuthenticationToken("1");

        $this->assertEquals($token, $actual);
    }

    /** @test */
    public function it_returns_authenticated_customer()
    {
        $obj2 = new \stdClass();
        $obj2->id = 2;
        $obj2->email = "you@piggy.nl";

        $input = ["data" => $obj2];

        $customer = new Customer();
        $customer->setId($obj2->id);
        $customer->setEmail($obj2->email);

        $piggyApi = $this->mockHttpClient($input);

        $piggyApi->setClientToken("1");
        $piggyApi->setAccessToken("1");

        $actual = $piggyApi->customer->profile();

        $this->assertInstanceOf(Customer::class, $actual);
        $this->assertEquals($customer, $actual);
    }
}