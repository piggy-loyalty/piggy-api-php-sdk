<?php

use GuzzleHttp\Psr7\Response;
use Piggy\Api\Exceptions\InvalidArgument;
use Piggy\Api\Exceptions\PiggyApiException;
use Tests\Piggy\Api\PiggyTestCase;

class ClientTest extends PiggyTestCase
{
    /**
     * @test
     * @throws PiggyApiException
     * @throws ReflectionException
     */
    public function it_throws_piggy_api_exception_when_client_authentication_failed()
    {
        $this->expectException(PiggyApiException::class);
        $this->expectExceptionMessage("Client authentication failed");

        $e = $this->createExceptionForMockedClient(401, "invalid_client", "Client authentication failed");

        $piggyApi = $this->mockHttpClient($e, true);

        $piggyApi->client->requestClientToken();
    }

    /**
     * @test
     * @throws PiggyApiException
     * @throws ReflectionException
     */
    public function it_returns_the_client_token_and_sets_client_token_property()
    {
        $expects = ["access_token" => "123"];
        $piggyApi = $this->mockHttpClient($expects);

        $actual = $piggyApi->client->requestClientToken();
        $clientToken = $piggyApi->getClientToken();

        $this->assertEquals($expects, $actual);
        $this->assertEquals($expects["access_token"], $clientToken);
    }

    /** @test */
    public function it_returns_invalid_argument_when_client_token_not_set()
    {
        $this->expectExceptionObject(new InvalidArgument("You must provide an access or client token."));
        $piggyApi = $this->mockHttpClient([]);

        $piggyApi->client->healthCheck();
    }

    /** @test */
    public function it_returns_response_of_http_request()
    {
        $piggyApi = $this->mockHttpClient([]);

        $piggyApi->setClientToken("123");

        $actual = $piggyApi->client->healthCheck();

        $this->assertInstanceOf(Response::class, $actual);
    }
}