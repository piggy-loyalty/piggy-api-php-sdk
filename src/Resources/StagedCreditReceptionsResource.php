<?php

namespace Piggy\Api\Resources;

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Mappers\StagedCreditReceptionMapper;
use Piggy\Api\Model\StagedCreditReception;
use Psr\Http\Message\ResponseInterface;

class StagedCreditReceptionsResource extends BaseResource
{

    protected $resourceUri = "/api/v1/oauth/clients/loyalty/staged-credit-receptions";

    /**
     * @param $shopId
     * @param $purchaseAmount
     * @param null $credits
     * @return StagedCreditReception
     * @throws PiggyApiException
     */
    public function create($shopId, $purchaseAmount, $credits = null)
    {
        $body = [
            "shop_id" => $shopId,
            "purchase_amount" => $purchaseAmount,
            "credits" => $credits,
        ];

        $response = $this->piggyApi->request("POST", $this->resourceUri, $body);

        $data = $this->getDataFromResponse($response);

        $mapper = new StagedCreditReceptionMapper();

        $stagedCreditReception = $mapper->mapFromResponse($data);

        return $stagedCreditReception;
    }

    /**
     * @param $hash
     * @param $email
     * @param string $locale
     * @return ResponseInterface
     * @throws PiggyApiException
     */
    public function send($hash, $email, $locale = "en")
    {
        $body = [
            "hash" => $hash,
            "email" => $email,
            "locale" => $locale
        ];

        $response = $this->piggyApi->request("POST", $this->resourceUri . "/mail", $body);

        return $response;
    }

    /**
     * @param $id
     * @return StagedCreditReception|ResponseInterface
     * @throws PiggyApiException
     */
    public function show($id)
    {
        $response = $this->piggyApi->request("GET", "/api/v2/oauth/clients/loyalty/staged-credit-receptions/" . $id);

        if($response instanceof PiggyApiException) {
            return $response;
        }

        $data = $this->getDataFromResponse($response);

        $mapper = new StagedCreditReceptionMapper();

        $stagedCreditReception = $mapper->mapFromResponse($data);

        return $stagedCreditReception;
    }

}