<?php

namespace Piggy\Api\Resources;

use Piggy\Api\PiggyApi;
use Psr\Http\Message\ResponseInterface;

/**
 * Class BaseResource
 * @package Piggy\Api\Resources
 */
abstract class BaseResource
{
    /**
     * @var PiggyApi $piggyApi
     */
    protected $piggyApi;

    /**
     * BaseResource constructor.
     * @param PiggyApi $piggyApi
     */
    public function __construct(PiggyApi $piggyApi)
    {
        $this->piggyApi = $piggyApi;
    }

    public function getDataFromResponse(ResponseInterface $response)
    {
        $contents = $response->getBody()->getContents();

        return $contents ? json_decode($contents)->data : null;
    }
}