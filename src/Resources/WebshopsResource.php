<?php

namespace Piggy\Api\Resources;

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Mappers\WebshopMapper;
use Piggy\Api\Mappers\WebshopsMapper;
use Piggy\Api\Model\Webshop;

/**
 * Class WebshopsResource
 * @package Piggy\Api\Resources
 */
class WebshopsResource extends BaseResource
{
    /**
     * @var string
     */
    protected $resourceUri = "/api/v1/oauth/clients/shops/web";

    /**
     * @return array
     * @throws PiggyApiException
     */
    public function index()
    {
        $response = $this->piggyApi->request("GET", $this->resourceUri, []);

        $mapper = new WebshopsMapper();

        return $mapper->mapFromResponse($this->getDataFromResponse($response));
    }

    /**
     * @param int $id
     * @return Webshop
     * @throws PiggyApiException
     */
    public function show(int $id)
    {
        $response = $this->piggyApi->request('GET', $this->resourceUri . "/" . $id, []);

        $mapper = new WebshopMapper();

        return $mapper->mapFromResponse($this->getDataFromResponse($response));
    }
}
