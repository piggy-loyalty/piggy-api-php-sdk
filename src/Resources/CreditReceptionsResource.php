<?php

namespace Piggy\Api\Resources;

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Mappers\CreditReceptionMapper;
use Piggy\Api\Model\CreditReception;
use Psr\Http\Message\ResponseInterface;

/**
 * Class CreditReceptionsResource
 * @package Piggy\Api\Resources\Entities
 */
class CreditReceptionsResource extends BaseResource
{
    /**
     * @var string
     */
    protected $resourceUri = "/api/v1/oauth/clients/loyalty/credit-receptions";

    /**
     * @param $email
     * @param $purchaseAmount
     * @param $shopId
     * @return CreditReception
     * @throws PiggyApiException
     */
    public function create($email, $purchaseAmount, $shopId)
    {
        $body = [
            "email" => $email,
            "purchase_amount" => $purchaseAmount,
            "shop_id" => $shopId,
        ];

        $response = $this->piggyApi->request("POST", $this->resourceUri, $body);

        $data = $this->getDataFromResponse($response);

        $mapper = new CreditReceptionMapper();

        $creditReception = $mapper->mapFromResponse($data);

        return $creditReception;
    }

    /**
     * @param $id
     * @return CreditReception|ResponseInterface
     * @throws PiggyApiException
     */
    public function show($id)
    {
        $response = $this->piggyApi->request("GET", "/api/v2/oauth/clients/loyalty/credit-receptions/" . $id);

        if($response instanceof PiggyApiException) {
            return $response;
        }

        $data = $this->getDataFromResponse($response);

        $mapper = new CreditReceptionMapper();

        $creditReception = $mapper->mapFromResponse($data);

        return $creditReception;
    }
}