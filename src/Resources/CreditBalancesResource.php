<?php

namespace Piggy\Api\Resources;

use Piggy\Api\Exceptions\PiggyApiException;

class CreditBalancesResource extends BaseResource
{
    /**
     * @var string
     */
    protected $resourceUri = "/api/v1/oauth/customer/credit-balance";

    /**
     * @param $shopId
     * @return |null
     * @throws PiggyApiException
     */
    public function show($shopId)
    {
        $response = $this->piggyApi->request("GET", $this->resourceUri . "/" . $shopId, []);

        return $this->getDataFromResponse($response);
    }
}