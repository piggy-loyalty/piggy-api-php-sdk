<?php

namespace Piggy\Api;

use Piggy\Api\Resources\CreditReceptionsResource;
use Piggy\Api\Resources\StagedCreditReceptionsResource;
use Piggy\Api\Resources\WebshopsResource;

/**
 * Trait SetsClientResources
 * @package Piggy\Api
 */
trait SetsClientResources
{
    /**
     * @var WebshopsResource
     */
    public $webshops;

    /**
     * @var StagedCreditReceptionsResource
     */
    public $stagedCreditReceptions;

    /**
     * @var CreditReceptionsResource
     */
    public $creditReceptions;

    /**
     * @param PiggyApi $piggyApi
     */
    protected function setResources(PiggyApi $piggyApi)
    {
        $this->webshops = new WebshopsResource($piggyApi);
        $this->stagedCreditReceptions = new StagedCreditReceptionsResource($piggyApi);
        $this->creditReceptions = new CreditReceptionsResource($piggyApi);
    }
}