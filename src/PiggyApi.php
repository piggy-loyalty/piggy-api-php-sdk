<?php

namespace Piggy\Api;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Piggy\Api\Exceptions\InvalidArgument;
use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Http\Client;
use Piggy\Api\Http\Customer;
use Psr\Http\Message\ResponseInterface;

/**
 * Class PiggyApi
 * @package Piggy\Api
 */
class PiggyApi
{
    /**
     * @var Client
     */
    public $client;

    /**
     * @var Customer
     */
    public $customer;

    /** @var GuzzleClient */
    public $httpClient;

    /**
     * @var string
     */
    public $baseUrl;

    /**
     * @var string
     */
    public $environment;

    /**
     * @var array
     */
    protected $headers = [
        'Accept' => 'application/json',
    ];

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var array
     */
    private $clientToken;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * PiggyApi constructor.
     * @param string $clientId
     * @param string $clientSecret
     * @param string $environment
     */
    public function __construct(string $clientId, string $clientSecret, string $environment = "production")
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;

        $this->environment = $environment;
        $this->determineBaseUrl($environment);

        $this->httpClient = new GuzzleClient();

        $this->client = new Client($this);
        $this->customer = new Customer($this);
    }

    /**
     * @param string $environment
     */
    private function determineBaseUrl(string $environment): void
    {
        switch($environment) {
            case "sandbox":
                $this->setBaseUrl("https://sandbox.piggy.nl");
                break;
            case "development":
                $this->setBaseUrl("https://dev.piggy.nl");
                break;
            case "production":
            default:
                $this->setBaseUrl("https://api.piggy.nl");
        }
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $query_options
     * @param bool
     * @return ResponseInterface
     * @throws PiggyApiException
     */
    public function request($method, $endpoint, $query_options = [], $authentication = true)
    {
        if ($authentication && empty($this->clientToken)) {
            // "token" or "client token or access token"?
            throw new InvalidArgument('You must provide an access or client token.');
        }

        $url = $this->generateUrl($endpoint);

        $options = [
            "headers" => $this->headers,
            "form_params" => $query_options,
        ];

        try {
            return $this->httpClient->request($method, $url, $options);
        } catch(GuzzleException $e) {
            throw PiggyApiException::createFromGuzzleException($e);
        }
    }

    /**
     * @param $endpoint
     * @return string
     */
    protected function generateUrl($endpoint): string
    {
        return $this->baseUrl . $endpoint;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addHeader($key, $value): void
    {
        $this->headers[$key] = $value;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return mixed
     */
    public function getClientSecret(): ?string
    {
        return $this->clientSecret;
    }

    /**
     * @return mixed
     */
    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    /**
     * @return mixed
     */
    public function getClientToken(): ?string
    {
        return $this->clientToken;
    }

    /**
     * @param string $clientToken
     * @return PiggyApi
     */
    public function setClientToken(string $clientToken): PiggyApi
    {
        $this->clientToken = $clientToken;
        $this->addHeader("Authorization", "Bearer {$clientToken}");
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    /**
     * @param $accessToken
     * @return PiggyApi
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
        $this->addHeader("Authorization", "Bearer {$accessToken}");
        return $this;
    }

    /**
     * @param string $baseUrl
     * @return PiggyApi
     */
    public function setBaseUrl(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret(string $clientSecret): void
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }
}