<?php

namespace Piggy\Api\Exceptions;

use InvalidArgumentException;

class InvalidArgument extends InvalidArgumentException
{
}