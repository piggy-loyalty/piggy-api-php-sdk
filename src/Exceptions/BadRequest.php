<?php

namespace Piggy\Api\Exceptions;

class BadRequest extends PiggyApiException
{
}