<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\Webshop;

/**
 * Class WebshopMapper
 * @package Piggy\Api\Mappers
 */
class WebshopMapper
{
    /**
     * @param $response
     * @return Webshop
     */
    public function mapFromResponse($response)
    {
        $webshop = new Webshop();

        $webshop->setId(isset($response->id) ? $response->id : null);
        $webshop->setName(isset($response->name) ? $response->name : null);

        $loyaltyProgramMapper = new LoyaltyProgramMapper();
        $webshop->setLoyaltyProgram($response->loyalty_program ? $loyaltyProgramMapper->mapFromResponse($response->loyalty_program) : null);

        return $webshop;
    }

    /**
     * @param Webshop $webshop
     * @return array
     */
    public function mapToResponse($webshop)
    {
        return [
            "id" => $webshop->getId(),
            "name" => $webshop->getName(),
        ];
    }
}