<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\CreditReception;

/**
 * Class CreditReceptionMapper
 * @package Piggy\Api\Mappers
 */
class CreditReceptionMapper
{
    /**
     * @param $response
     * @return CreditReception
     */
    public function mapFromResponse($response)
    {
        $creditReception = new CreditReception();

        $customerMapper = new CustomerMapper();

        $creditReception->setId(isset($response->id) ? $response->id : 0);
        $creditReception->setCredits(isset($response->credits) ? $response->credits : 0);
        $creditReception->setPurchaseAmount(isset($response->purchase_amount) ? $response->purchase_amount : 0);
        $creditReception->setCreatedAt(isset($response->created_at) ? $response->created_at : null);
        $creditReception->setCustomer(isset($response->member) ? $customerMapper->mapFromResponse($response->member) : null);

        return $creditReception;
    }

    /**
     * @param CreditReception $creditReception
     * @return array
     */
    public function mapToResponse($creditReception)
    {
        $customer = new CustomerMapper();

        return [
            "id" => $creditReception->getId(),
            "credits" => $creditReception->getCredits(),
            "customer" => $customer->mapToResponse($creditReception->getCustomer()),
            "purchase_amount" => $creditReception->getPurchaseAmount(),
            "created_at" => $creditReception->getCreatedAt(),
        ];
    }
}