<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\StagedCreditReception;

/**
 * Class StagedCreditReceptionMapper
 * @package Piggy\Api\Mappers
 */
class StagedCreditReceptionMapper
{
    /**
     * @param $response
     * @return StagedCreditReception
     */
    public function mapFromResponse($response)
    {
        $staged = new StagedCreditReception();

        $creditReceptionsMapper = new CreditReceptionMapper();

        $staged->setHash(isset($response->hash) ? $response->hash : "");
        $staged->setCredits(isset($response->credits) ? $response->credits : "");
        $staged->setCreatedAt(isset($response->created_at) ? $response->created_at : "");
        $staged->setCreditReception(isset($response->credit_reception) ? $creditReceptionsMapper->mapFromResponse($response->credit_reception) : null);

        return $staged;
    }

    /**
     * @param StagedCreditReception $stagedCreditReception
     * @return array
     */
    public function mapToResponse($stagedCreditReception)
    {
        return [
            "hash" => $stagedCreditReception->getHash(),
            "credits" => $stagedCreditReception->getCredits(),
            "shop" => $stagedCreditReception->getShop(),
        ];
    }
}
