<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\Customer;

/**
 * Class CustomerMapper
 * @package Piggy\Api\Mappers
 */
class CustomerMapper
{
    /**
     * @param $response
     * @return Customer
     */
    public function mapFromResponse($response)
    {
        $customer = new Customer();

        $customer->setId(isset($response->id) ? $response->id : 0);
        $customer->setEmail(isset($response->email) ? $response->email : "");
        $customer->setFirstName(isset($response->first_name) ? $response->first_name : "");
        $customer->setLastName(isset($response->last_name) ? $response->last_name : "");

        return $customer;
    }

    /**
     * @param Customer $customer
     * @return array
     */
    public function mapToResponse($customer)
    {
        return [
            "id" => $customer->getId(),
            "email" => $customer->getEmail(),
            "first_name" => $customer->getFirstName(),
            "last_name" => $customer->getLastName(),
        ];
    }
}