<?php

namespace Piggy\Api\Mappers;

/**
 * Class StagedCreditReceptionsMapper
 * @package Piggy\Api\Mappers
 */
class StagedCreditReceptionsMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $stagedCreditReceptions = [];
        $stagedCreditReceptionMapper = new StagedCreditReceptionMapper();

        foreach ($response as $item) {
            $stagedCreditReceptions[] = $stagedCreditReceptionMapper->mapFromResponse($item);
        }

        return $stagedCreditReceptions;
    }

    /**
     * @param $stagedCreditReceptions
     * @return array
     */
    public function mapToResponse($stagedCreditReceptions)
    {
        $response = [];
        $stagedCreditReceptionMapper = new StagedCreditReceptionMapper();

        foreach ($stagedCreditReceptions as $item) {
            $response[] = $stagedCreditReceptionMapper->mapToResponse($item);
        }

        return $response;
    }
}
