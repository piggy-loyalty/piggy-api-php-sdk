<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\LoyaltyProgram;
use Piggy\Api\Model\Webshop;

/**
 * Class LoyaltyProgramMapper
 * @package Piggy\Api\Mappers
 */
class LoyaltyProgramMapper
{
    /**
     * @param $response
     * @return LoyaltyProgram
     */
    public function mapFromResponse($response)
    {
        $loyaltyProgram = new LoyaltyProgram();
        
        $loyaltyProgram->setId(isset($response->id) ? $response->id : null);
        $loyaltyProgram->setName(isset($response->name) ? $response->name : null);

        return $loyaltyProgram;
    }

    /**
     * @param $loyaltyProgram
     * @return array
     */
    public function mapToResponse($loyaltyProgram)
    {
        return [
            "id" => $loyaltyProgram->getId(),
            "name" => $loyaltyProgram->getName(),
        ];
    }
}