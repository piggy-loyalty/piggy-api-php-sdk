<?php

namespace Piggy\Api\Mappers;

/**
 * Class WebshopsMapper
 * @package Piggy\Api\Mappers
 */
class WebshopsMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $webshops = [];
        $webshopMapper = new WebshopMapper();

        foreach ($response as $webshop) {
            $webshops[] = $webshopMapper->mapFromResponse($webshop);
        }

        return $webshops;
    }

    /**
     * @param $webshops
     * @return array
     */
    public function mapToResponse($webshops)
    {
        $response = [];
        $webshopMapper = new WebshopMapper();

        foreach ($webshops as $webshop) {
            $response[] = $webshopMapper->mapToResponse($webshop);
        }

        return $response;
    }

}