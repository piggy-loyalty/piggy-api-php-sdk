<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\Group;
use Piggy\Api\Model\OAuthToken;
use Piggy\Api\Model\StagedCreditReception;

/**
 * Class OAuthTokenMapper
 * @package Piggy\Api\Mappers
 */
class OAuthTokenMapper
{
    /**
     * @param $response
     * @return OAuthToken
     */
    public function mapFromResponse($response)
    {
        $token = new OAuthToken();

        $token->setAccessToken($response->access_token);
        $token->setRefreshToken($response->refresh_token);
        $token->setExpiresIn($response->expires_in);

        return $token;
    }

    /**
     * @param OAuthToken $OAuthToken
     * @return array
     */
    public function mapToResponse($OAuthToken)
    {
        return [
            "accessToken" => $OAuthToken->getAccessToken(),
            "refreshToken" => $OAuthToken->getRefreshToken(),
            "expiresIn" => $OAuthToken->getExpiresIn(),
        ];
    }
}