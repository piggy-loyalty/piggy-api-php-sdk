<?php

namespace Piggy\Api\Mappers;


/**
 * Class GiftcardProgramsMapper
 * @package Piggy\Api\Mappers
 */
class GroupsMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $groups = [];
        $groupMapper = new GroupMapper();

        foreach ($response as $item) {
            $groups[] = $groupMapper->mapFromResponse($item);
        }

        return $groups;
    }

    /**
     * @param $groups
     * @return array
     */
    public function mapToResponse($groups)
    {
        $response = [];
        $groupMapper = new GroupMapper();

        foreach ($groups as $item) {
            $response[] = $groupMapper->mapToResponse($item);
        }

        return $response;
    }
}
