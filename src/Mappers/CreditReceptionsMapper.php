<?php

namespace Piggy\Api\Mappers;

/**
 * Class CreditReceptionsMapper
 * @package Piggy\Api\Mappers
 */
class CreditReceptionsMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $creditReceptions = [];
        $creditReceptionMapper = new CreditReceptionMapper();

        foreach ($response as $item) {
            $creditReceptions[] = $creditReceptionMapper->mapFromResponse($item);
        }

        return $creditReceptions;
    }

    /**
     * @param $creditReceptions
     * @return array
     */
    public function mapToResponse($creditReceptions)
    {
        $response = [];
        $creditReceptionMapper = new CreditReceptionMapper();

        foreach ($creditReceptions as $item) {
            $response[] = $creditReceptionMapper->mapToResponse($item);
        }

        return $response;
    }
}
