<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\GiftcardProgram;

/**
 * Class GiftcardProgramMapper
 * @package Piggy\Api\Mappers
 */
class GiftcardProgramMapper
{
    /**
     * @param $response
     * @return GiftcardProgram
     */
    public function mapFromResponse($response)
    {
        $gp = new GiftcardProgram();
        $groupsMapper = new GroupsMapper();

        $gp->setId(isset($response['id']) ? $response['id'] : "");
        $gp->setName(isset($response['name']) ? $response['name'] : "");
        $gp->setSettles(isset($response['settles']) ? $response['settles'] : "");
        $gp->setExpirationDays(isset($response['expiration_days']) ? $response['expiration_days'] : "");
        $gp->setMaxAmount(isset($response['max_amount']) ? $response['max_amount'] : "");
        $gp->setGroups($groupsMapper->mapFromResponse($response['groups']));

        return $gp;
    }

    /**
     * @param GiftcardProgram $giftcard
     * @return array
     */
    public function mapToResponse($giftcard)
    {
        return [
            "id" => $giftcard->getId(),
            "name" => $giftcard->getName(),
            "settles" => $giftcard->getSettles(),
            "expiration_days" => $giftcard->getExpirationDays(),
            "max_amount" => $giftcard->getMaxAmount(),
            "groups" => $giftcard->getGroups(),
        ];
    }
}
