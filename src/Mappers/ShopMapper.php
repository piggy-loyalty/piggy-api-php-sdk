<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\Shop;

/**
 * Class ShopMapper
 * @package Piggy\Api\Mappers
 */
class ShopMapper
{
    /**
     * @param $response
     * @return Shop
     */
    public function mapFromResponse($response)
    {
        $shop = new Shop();

        $shop->setId(isset($response->id) ? $response->id : "");
        $shop->setName(isset($response->name) ? $response->name : "");

        return $shop;
    }

    /**
     * @param Shop $shop
     * @return array
     */
    public function mapToResponse($shop)
    {
        return [
            "id" => $shop->getId(),
            "name" => $shop->getName(),
        ];
    }
}
