<?php

namespace Piggy\Api\Mappers;

/**
 * Class ShopsMapper
 * @package Piggy\Api\Mappers
 */
class ShopsMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $shops = [];
        $shopMapper = new ShopMapper();

        foreach ($response as $item) {
            $shops[] = $shopMapper->mapFromResponse($item);
        }

        return $shops;
    }

    /**
     * @param $shops
     * @return array
     */
    public function mapToResponse($shops)
    {
        $response = [];
        $shopMapper = new ShopMapper();

        foreach ($shops as $item) {
            $response[] = $shopMapper->mapToResponse($item);
        }

        return $response;
    }
}
