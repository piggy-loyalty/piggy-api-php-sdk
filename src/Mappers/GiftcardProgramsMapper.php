<?php

namespace Piggy\Api\Mappers;


use Piggy\Api\Model\GiftcardProgram;

/**
 * Class GiftcardProgramsMapper
 * @package Piggy\Api\Mappers
 */
class GiftcardProgramsMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $giftcardPrograms = [];
        $giftcardProgramMapper = new GiftcardProgramMapper();

        foreach ($response as $item) {
            $giftcardPrograms[] = $giftcardProgramMapper->mapFromResponse($item);
        }

        return $giftcardPrograms;
    }

    /**
     * @param GiftcardProgram$gifcardPrograms $gifcardPrograms
     * @return array
     */
    public function mapToResponse($gifcardPrograms)
    {
        $response = [];
        $giftcardProgramMapper = new GiftcardProgramMapper();

        foreach ($gifcardPrograms as $item) {
            $response[] = $giftcardProgramMapper->mapToResponse($item);
        }

        return $response;
    }
}
