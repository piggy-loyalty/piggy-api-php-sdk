<?php

namespace Piggy\Api\Mappers;

/**
 * Class CustomersMapper
 * @package Piggy\Api\Mappers
 */
class CustomersMapper
{
    /**
     * @param $response
     * @return array
     */
    public function mapFromResponse($response)
    {
        $customers = [];
        $customerMapper = new CustomerMapper();

        foreach ($response as $item) {
            $customers[] = $customerMapper->mapFromResponse($item);
        }

        return $customers;
    }

    /**
     * @param $customers
     * @return array
     */
    public function mapToResponse($customers)
    {
        $response = [];
        $customerMapper = new CustomerMapper();

        foreach ($customers as $item) {
            $response[] = $customerMapper->mapToResponse($item);
        }

        return $response;
    }
}
