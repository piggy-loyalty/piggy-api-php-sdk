<?php

namespace Piggy\Api\Mappers;

use Piggy\Api\Model\Group;

/**
 * Class GroupMapper
 * @package Piggy\Api\Mappers
 */
class GroupMapper
{
    /**
     * @param $response
     * @return Group
     */
    public function mapFromResponse($response)
    {
        $group = new Group();
        $shopsMapper = new ShopsMapper();

        $group->setId(isset($response['id']) ? $response['id'] : "");
        $group->setShops($shopsMapper->mapFromResponse($response['shops']));

        return $group;
    }

    /**
     * @param Group $group
     * @return array
     */
    public function mapToResponse($group)
    {
        $shopsMapper = new ShopsMapper();

        return [
            "id" => $group->getId(),
            "shops" => $shopsMapper->mapToResponse($group->getShops()),
        ];
    }
}
