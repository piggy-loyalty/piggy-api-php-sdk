<?php

namespace Piggy\Api\Model;

/**
 * Class StagedCreditReception
 * @package Piggy\Api\Model
 */
class StagedCreditReception
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * @var
     */
    protected $credits;

    /**
     * @var CreditReception | null
     */
    protected $creditReception;

    /**
     * @var string
     */
    protected $createdAt;

    /**
     * StagedCreditReception constructor.
     */
    public function __construct()
    {
        $this->creditReception = new CreditReception();
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param $credits
     */
    public function setCredits($credits): void
    {
        $this->credits = $credits;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string|null $createdAt
     */
    public function setCreatedAt(?string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return CreditReception|null
     */
    public function getCreditReception()
    {
        return $this->creditReception;
    }

    /**
     * @param CreditReception|null $creditReception
     */
    public function setCreditReception(?CreditReception $creditReception): void
    {
        $this->creditReception = $creditReception;
    }
}