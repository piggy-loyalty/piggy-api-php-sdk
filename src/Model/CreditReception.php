<?php

namespace Piggy\Api\Model;

use DateTime;

/**
 * Class CreditReception
 * @package Piggy\Api\Model
 */
class CreditReception
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var
     */
    protected $credits;

    /**
     * @var float|null
     */
    protected $purchaseAmount;

    /**
     * NOTE: This needs to big Member refactor.
     * @var Customer|null
     */
    protected $customer;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param $credits
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
    }

    /**
     * @return float|null
     */
    public function getPurchaseAmount(): ?float
    {
        return $this->purchaseAmount;
    }

    /**
     * @param float|null $purchaseAmount
     */
    public function setPurchaseAmount(?float $purchaseAmount)
    {
        $this->purchaseAmount = $purchaseAmount;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer|null $customer
     */
    public function setCustomer(?Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}