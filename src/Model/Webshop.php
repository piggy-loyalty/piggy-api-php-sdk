<?php

namespace Piggy\Api\Model;

/**
 * Class Webshop
 * @package Piggy\Api\Model
 */
class Webshop
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var LoyaltyProgram|null $loyaltyProgram
     */
    protected $loyaltyProgram;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return LoyaltyProgram|null
     */
    public function getLoyaltyProgram(): ?LoyaltyProgram
    {
        return $this->loyaltyProgram;
    }

    /**
     * @param LoyaltyProgram|null $loyaltyProgram
     */
    public function setLoyaltyProgram(?LoyaltyProgram $loyaltyProgram): void
    {
        $this->loyaltyProgram = $loyaltyProgram;
    }

}