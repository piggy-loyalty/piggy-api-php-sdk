<?php

namespace Piggy\Api\Model;

/**
 * Class GiftcardProgram
 * @package Piggy\Api\Model
 */
class GiftcardProgram
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $settles;

    /**
     * @var int
     */
    protected $expirationDays;

    /**
     * @var int
     */
    protected $maxAmount;

    /**
     * @var array
     */
    protected $groups;

    /**
     * GiftcardProgram constructor.
     */
    public function __construct()
    {
        $this->groups = [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getSettles(): ?int
    {
        return $this->settles;
    }

    /**
     * @param int $settles
     */
    public function setSettles(?int $settles): void
    {
        $this->settles = $settles;
    }

    /**
     * @return int
     */
    public function getExpirationDays(): ?int
    {
        return $this->expirationDays;
    }

    /**
     * @param int $expirationDays
     */
    public function setExpirationDays(?int $expirationDays): void
    {
        $this->expirationDays = $expirationDays;
    }

    /**
     * @return int
     */
    public function getMaxAmount(): int
    {
        return $this->maxAmount;
    }

    /**
     * @param int $maxAmount
     */
    public function setMaxAmount(?int $maxAmount): void
    {
        $this->maxAmount = $maxAmount;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }

}