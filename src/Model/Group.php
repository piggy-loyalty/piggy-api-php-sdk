<?php

namespace Piggy\Api\Model;

/**
 * Class Group
 * @package Piggy\Api\Model
 */
class Group
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var array
     */
    protected $shops;

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->shops = [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getShops(): array
    {
        return $this->shops;
    }

    /**
     * @param array $shops
     */
    public function setShops(array $shops): void
    {
        $this->shops = $shops;
    }
}