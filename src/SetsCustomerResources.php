<?php

namespace Piggy\Api;

use Piggy\Api\Resources\CreditBalancesResource;

/**
 * Trait SetsClientResources
 * @package Piggy\Api
 */
trait SetsCustomerResources
{
    /**
     * @var CreditBalancesResource
     */
    public $creditBalances;

    /**
     * @param PiggyApi $piggyApi
     */
    protected function setResources(PiggyApi $piggyApi)
    {
        $this->creditBalances = new CreditBalancesResource($piggyApi);
    }
}