<?php

namespace Piggy\Api\Http;

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\PiggyApi;
use Piggy\Api\SetsClientResources;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Client
 * @package Piggy\Api\Http
 */
class Client
{
    use SetsClientResources;

    /** @var PiggyApi $piggyApi */
    public $piggyApi;

    /**
     * Client constructor.
     * @param PiggyApi $piggyApi
     */
    public function __construct(PiggyApi $piggyApi)
    {
        $this->piggyApi = $piggyApi;
        $this->setResources($this->piggyApi);
    }

    /**
     * @return mixed
     * @throws PiggyApiException
     */
    public function requestClientToken()
    {
        $body = [
            "grant_type" => "client_credentials",
            "client_id" => $this->piggyApi->getClientId(),
            "client_secret" => $this->piggyApi->getClientSecret()
        ];

        $response = $this->piggyApi->request("POST", "/oauth/token", $body, false);

        $clientToken = json_decode($response->getBody()->getContents(), true);

        $this->piggyApi->addHeader("Authorization", "Bearer {$clientToken['access_token']}"); // maybe not do this, but looks not bad actually
        $this->piggyApi->setClientToken($clientToken['access_token']); // maybe not do this, but looks not bad actually

        return $clientToken;
    }

    /**
     * @return ResponseInterface
     * @throws PiggyApiException
     */
    public function healthCheck()
    {
        $response = $this->piggyApi->request("GET", "/api/v1/oauth/clients");

        return $response;
    }
}