<?php

namespace Piggy\Api\Http;

use Piggy\Api\Exceptions\PiggyApiException;
use Piggy\Api\Mappers\CustomerMapper;
use Piggy\Api\Mappers\OAuthTokenMapper;
use Piggy\Api\Model\OAuthToken;
use Piggy\Api\PiggyApi;
use Piggy\Api\SetsCustomerResources;

/**
 * Class Customer
 * @package Piggy\Api\Http
 */
class Customer
{
    use SetsCustomerResources;

    /**
     * @var PiggyApi
     */
    public $piggyApi;

    /**
     * @var string
     */
    private $redirectUri;

    /**
     * Customer constructor.
     * @param PiggyApi $piggyApi
     */
    public function __construct(PiggyApi $piggyApi)
    {
        $this->piggyApi = $piggyApi;
        $this->setResources($this->piggyApi);
    }

    /**
     * @param string $redirectUri
     * @return string
     */
    public function generateAuthenticationUrl(string $redirectUri): string
    {
        $clientId = $this->piggyApi->getClientId();

        $this->setRedirectUri($redirectUri);

        switch ($this->piggyApi->environment) {
            case "development":
                $baseUrl = "https://customer.piggyloyalty.nl";
                break;
            case "production":
            default:
                $baseUrl = "https://customer.piggy.nl";
        }

        $url = "{$baseUrl}/oauth?client_id={$clientId}&redirect_uri={$redirectUri}&response_type=code&scope=";

        return $url;
    }

    /**
     * @param string $code
     * @return OAuthToken
     * @throws PiggyApiException
     */
    public function requestAuthenticationToken(string $code)
    {
        $body = [
            "grant_type" => "authorization_code",
            "client_id" => $this->piggyApi->getClientId(),
            "client_secret" => $this->piggyApi->getClientSecret(),
            "code" => $code,
            "redirect_uri" => $this->getRedirectUri()
        ];

        $response = $this->piggyApi->request("POST", "/oauth/token", $body, false);
        $data = json_decode($response->getBody()->getContents());

        $mapper = new OAuthTokenMapper();

        return $mapper->mapFromResponse($data);
    }

    /**
     * @return \Piggy\Api\Model\Customer
     * @throws PiggyApiException
     */
    public function profile()
    {
        $response = $this->piggyApi->request("GET", "/api/v1/oauth/customer/profile", []);

        $data = json_decode($response->getBody()->getContents())->data;

        $mapper = new CustomerMapper();

        return $mapper->mapFromResponse($data);
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    /**
     * @param string $redirectUri
     */
    public function setRedirectUri(string $redirectUri): void
    {
        $this->redirectUri = $redirectUri;
    }
}